let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Front Assets
 |--------------------------------------------------------------------------
 */



mix.setPublicPath('public/')
mix.js('src/main.js', 'public/js/app.js');

mix.sass('src/assets/scss/app.scss', 'public/css/app.css')

