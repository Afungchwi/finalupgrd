import Vue from 'vue'
import App from './App.vue'

//layout

import defaultLayout from './components/layout/default-layout'


import VueRouter from 'vue-router'
//vue router
Vue.use(VueRouter);
import routes from './router/router'

Vue.component('default-layout',defaultLayout)



Vue.config.productionTip = false

const router = new VueRouter({
    mode: 'history',
    routes
});





new Vue({
    router,
  render: h => h(App),

}).$mount('#app')
